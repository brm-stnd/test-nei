import React from "react";
import PropTypes from "prop-types";

const PersonList = ({ children }) => (
  <div className="PersonList">{children}</div>
);

PersonList.propTypes = {
  children: PropTypes.node
};

PersonList.defaultProps = {
  children: []
};

export default PersonList;
