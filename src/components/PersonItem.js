import React from "react";
import PropTypes from "prop-types";

import { Button, OverlayTrigger, Popover, Container, Row, Col } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faClock, faPhoneAlt, faEnvelope, faTrash } from '@fortawesome/free-solid-svg-icons'

const typeStatus = (status) => {
  switch (status) {
    case 'Warmup':
      return 'danger'
    case '1:1':
      return 'success'
    case 'VIP':
      return 'info'

    default:
      break;
  }
}

const typeColor = (status) => {
  switch (status) {
    case 'Warmup':
      return '#C82333'
    case '1:1':
      return '#218838'
    case 'VIP':
      return '#138496'

    default:
      break;
  }
}

const PersonItem = ({ dataPerson, onClickDelete }) => (
  <div className="PersonItem">
    <OverlayTrigger
      placement="bottom"
      delay={{ show: 250, hide: 400 }}
      overlay={
        <Popover id="popover-contained">
          <Popover.Content style={{ width: '20em' }}>
            <Container>
              <Row>
                <Col sm={10}>
                  <strong>{dataPerson.name}</strong>
                </Col>
                <Col sm={2}>
                  <FontAwesomeIcon icon={faEdit} />
                </Col>
              </Row>
              <Row className="mt-2">
                <Col sm={1}>
                  <div style={{
                    width: '20px',
                    height: '20px',
                    background: typeColor(dataPerson.status),
                    borderRadius: '5px'
                  }}>
                  </div>
                </Col>
                <Col sm>
                  <label>{dataPerson.status}</label>
                </Col>
              </Row>
              <Row>
                <Col sm={1}>
                  <FontAwesomeIcon icon={faClock} />
                </Col>
                <Col sm>
                  <label>{dataPerson.time}</label>
                </Col>
              </Row>
              <Row className="mt-2">
                <Col sm={12}>
                  <strong>Contact Detail</strong>
                </Col>
              </Row>
              <Row className="mt-1">
                <Col sm={1}>
                  <FontAwesomeIcon icon={faPhoneAlt} />
                </Col>
                <Col sm>
                  <label>{dataPerson.phone}</label>
                </Col>
              </Row>
              <Row>
                <Col sm={1}>
                  <FontAwesomeIcon icon={faEnvelope} />
                </Col>
                <Col sm>
                  <label>{dataPerson.email}</label>
                </Col>
              </Row>
            </Container>
          </Popover.Content>
        </Popover>
      }
    >
      <Button style={{ width: '100%' }} className="mt-2" variant={typeStatus(dataPerson.status)} >
        {dataPerson.name}
        <FontAwesomeIcon
          type="button"
          className="pull-right"
          onClick={onClickDelete}
          style={{ float: 'right', margin: '3px' }}
          icon={faTrash}
        />
      </Button>
    </OverlayTrigger>
  </div >
);

PersonItem.propTypes = {
  name: PropTypes.string,
  phone: PropTypes.string,
  onClickDelete: PropTypes.func
};

PersonItem.defaultProps = {
  name: "Anonymous",
  phone: "+62 xx xxx xxx xxx",
  onClickDelete: () => { }
};

export default PersonItem;
