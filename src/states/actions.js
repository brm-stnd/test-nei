let currentID = 0;

export function addPerson(person) {
  const { name, status, time, email, phone } = person;
  currentID += 1;

  return {
    type: "ADD_PERSON",
    id: currentID,
    name,
    status,
    time,
    email,
    phone
  };
}

export function removePerson(id) {
  return {
    type: "REMOVE_PERSON",
    id
  };
}
