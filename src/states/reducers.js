import { combineReducers } from "redux";

function persons(state = [], action) {
  switch (action.type) {
    case "ADD_PERSON":
      return [
        ...state,
        {
          id: action.id,
          name: action.name,
          status: action.status,
          time: action.time,
          email: action.email,
          phone: action.phone
        }
      ];
    case "REMOVE_PERSON":
      return state.filter(person => person.id !== action.id);
    default:
      return state;
  }
}

const Reducers = combineReducers({
  persons
});

export default Reducers;
