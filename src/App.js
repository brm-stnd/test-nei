import React, { Component } from "react";
import { connect } from "react-redux";

import PersonList from "./components/PersonList";
import PersonItem from "./components/PersonItem";
import { addPerson, removePerson } from "./states/actions";
import "./assets/css/App.css";

class App extends Component {
  constructor(props) {
    super(props);
    const { addNewPerson } = this.props;

    this.state = {
      name: "",
      status: "Warmup",
      time: "",
      email: "",
      phone: ""
    };

    const initialData = [{
      name: 'William Doe',
      status: 'Warmup',
      time: '04:30 AM - 05:AM',
      phone: '+8434 2323232323',
      email: 'williamdoe@mail.com'
    },
    {
      name: 'John Doe',
      status: '1:1',
      time: '04:30 AM - 05:AM',
      phone: '+8434 2323232323',
      email: 'williamdoe@mail.com'
    },
    {
      name: 'Peter Doe',
      status: 'VIP',
      time: '04:30 AM - 05:AM',
      phone: '+8434 2323232323',
      email: 'williamdoe@mail.com'
    }]
    localStorage.setItem('listPerson', JSON.stringify(initialData));

    let listPerson = JSON.parse(localStorage.getItem('listPerson'))

    if (listPerson && listPerson.length > 0) {
      listPerson.map((person) => {
        addNewPerson(person)
      })
    }

  }

  showTooltip = bool => {
    this.setState({ tooltipShow: bool })
  }

  render() {
    const { name, status, time, email, phone } = this.state;
    
    const { persons, addNewPerson, removeExistingPerson } = this.props;

    return (
      <div className="App">
        <div className="App__form">
          <input
            type="text"
            value={name}
            onChange={event => this.setState({ name: event.target.value })}
            className="App__input"
            placeholder="Name"
          />
          <br />
          <select
            value={status}
            onChange={event => this.setState({ status: event.target.value })}
            className="App__input"
          >
            <option value="Warmup">Warmup</option>
            <option value="1:1">1:1</option>
            <option value="VIP">VIP</option>
          </select>
          <br />
          <input
            type="text"
            value={time}
            onChange={event => this.setState({ time: event.target.value })}
            className="App__input"
            placeholder="Time"
          />
          <br />
          <input
            type="text"
            value={phone}
            onChange={event => this.setState({ phone: event.target.value })}
            className="App__input"
            placeholder="Phone"
          />
          <br />
          <input
            type="text"
            value={email}
            onChange={event => this.setState({ email: event.target.value })}
            className="App__input"
            placeholder="Email"
          />
          <br />
          <button
            type="button"
            onClick={() => {
              if (!name || !status || !time || !email || !phone) {
                alert("Field cannot be empty !");
                return;
              }

              this.setState({ name: "", status: "Warmup", time: "", email: "", phone: "", });
              addNewPerson(this.state);
            }}
            className="App__button"
          >
            Add Person
          </button>
        </div>
        <PersonList>
          {persons.map(person => {
            return (
              <PersonItem
                dataPerson={person}
                onClickDelete={() => removeExistingPerson(person.id)}
              />
            );
          })}
        </PersonList>
      </div>
    );
  }
}


const mapStateToProps = ({ persons }) => ({
  persons
});

const mapDispatchToProps = dispatch => ({
  addNewPerson: person => {
    dispatch(addPerson(person));
  },
  removeExistingPerson: personId => {
    dispatch(removePerson(personId));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
